from filterlib import Filter, filter_from_repr
from quicktests import TestBase, print_report, get_report


class ControlObject:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


class Test(TestBase):
    def test_filters_recursion(self):
        class Obj:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)

        f = Filter(a__b__c__d__e__eq__=1)
        obj = Obj(a=Obj(b=Obj(c=Obj(d=Obj(e=1)))))
        assert obj.a.b.c.d.e == 1, "Object declaration for this test does not work"
        assert f == obj, "Filters do not work with recursion."

    def test_filters(self):
        f = Filter(header__recipient__eq__="none",
                   body__content__eq__="None")
        m = ControlObject(header=ControlObject(recipient="none",
                                               timestamp=0),
                          body=ControlObject(content="None"))
        assert f == m, "Filter does not allow more attributes then it has itself."

    def test_filter_with_filters_as_attribute(self):
        class Obj:
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)

        assert Filter(f=Filter(a__eq__=1)) == Obj(a=1), "filters cannot contain filters"

    def test_filters_with_wrong_attributes(self):
        f = Filter(header__recipient__eq__="someone")
        m = ControlObject(header=ControlObject(recipient="none",
                                               timestamp=0),
                          body=ControlObject(content="None"))
        assert not f == m, \
            "Filter does allow wrong attribute values."

    def test_filters_too_many_restrictions(self):
        f = Filter(recipient__eq__="someone",
                   timestamp__gte__=0,
                   content__eq__="None",
                   something_else__eq__="Does not matter")
        m = ControlObject(header=ControlObject(recipient="none",
                                               timestamp=0),
                          body=ControlObject(content="None"))
        assert not f == m, \
            "Filter does allow missing attribute."

    def test_filters_wrong_number_comparison(self):
        f = Filter(timestamp__lte__=0)
        m = ControlObject(header=ControlObject(recipient="none",
                                               timestamp=1),
                          body=ControlObject(content="None"))
        assert not f == m, \
            "Filter does allow wrong comparison."

    def test_filters_contains_comparison(self):
        f = Filter(recipient__contains__="n")
        m = ControlObject(header=ControlObject(recipient="none",
                                               timestamp=1),
                          body=ControlObject(content="None"))
        assert not f == m, \
            "Filter fails at __contains__."

    def test_filter_to_sql_string(self):
        assert str(Filter(a__eq__="b", c__lte__=1)) == "a='b' AND c<=1", "Filter is not outputted as string correctly"

    def test_filters_with_underscores(self):
        assert str(Filter(a_b__eq__="b")) == "a_b='b'", "Filter is not outputted as string correctly"

    def test_filter_with_operator_as_sql(self):
        assert str(Filter(a__eq__="b", c__lte__=1, operator="OR")) == "a='b' OR c<=1", "Filters with operator fail"
        assert str(Filter(a__eq__="b", c__lte__=1, operator="AND")) == "a='b' AND c<=1", "Filters with operator fail"

    def test_filter_in_filter(self):
        string = str(Filter(a__eq__="a", b=Filter(c__lte__=1), operator="OR"))
        assert string == "a='a' OR c<=1", f"Filter in filter fail with str(): {string}"

    def test_filter_with_filter__repr__(self):
        f = Filter(operator="OR",
                   a=Filter(desc__contains__=" "))
        expected = '<Filter: {"a": "<Filter: {\\\"desc__contains__\\\": \\\" \\\", \\\"operator\\\": \\\"AND\\\"}>", "operator": "OR"}>'
        assert expected == repr(f), "Filters in filters are not compatible with repr()"

    def test_filters_in_filter(self):
        string = str(Filter(a__eq__="a", b=Filter(c__lte__=1, d__eq__='d'), operator="OR"))
        assert string == "a='a' OR (c<=1 AND d='d')", f"Multiple filters in filter fail: {string}"

    def test_filter_bool(self):
        assert not bool(Filter()), "Empty filter does not return False"
        assert bool(Filter(a__eq__=1)), "Usable filter does not return True"

    def test_filter_to_and_from_representation(self):
        f = Filter(a__eq__='b')
        assert f.__repr__() == '<Filter: {"a__eq__": "b", "operator": "AND"}>', f"Filter representation does not work {f.__repr__()}"
        assert filter_from_repr(f.__repr__()).__dict__ == f.__dict__, "Filter cannot be restored from representation"

    def test_detect_space_in_string(self):
        class Obj:
            def __init__(self, content: str):
                self.content = content

        f = Filter(content__contains__=" ")
        o = Obj("example text")
        assert f == o, "Filter does not detect spaces in string"

    def test_filtering_strings(self):
        f = Filter(__contains__=" ")
        assert f == "abc def", "Original strings are not supported"
        assert f != "abcdef", "Original strings are not supported"

    def test_operator_or(self):
        f = Filter(operator="OR",
                   a=Filter(__contains__="a"),
                   b=Filter(__contains__="b"))
        assert f.operator == "OR", "Operator is not set correctly"
        assert f == "a", "\"OR\" as operator does not work"

    def test_operator_and(self):
        s = "ab"
        f = Filter(operator="AND",
                   a=Filter(__contains__="a"),
                   b=Filter(__contains__="b"))
        assert f.operator == "AND", "Operator is not set correctly"
        assert f == "ab", "\"AND\" as operator does not work"
        assert f != "a" and f != "b", "\"AND\" as operator does not work"


def main():
    # run all tests
    t = Test()
    print_report(t)
    exit_status = int(len(get_report(t)))
    exit(exit_status)


if __name__ == "__main__":
    main()
